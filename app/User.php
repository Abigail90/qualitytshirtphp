<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements Authenticatable
{
    //
    protected  $table = 'users';
    use \Illuminate\Auth\Authenticatable;

    protected $fillable = ['username','email','password','name','phone','address','post','city','country'];

    protected $hidden = ['password','remember_token'];





}
