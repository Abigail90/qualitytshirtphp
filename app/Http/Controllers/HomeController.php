<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'asc')->take(6)->get();
        $categories = Category::all();
        return view('home',[
            'products' => $products,'categories' => $categories
        ]);
    }

    public function contactPage()
    {
        return view('home/contact');
    }

    public function viewProduct($id)
    {
        $product = Product::find($id);
        return view('home/product_view',['product' => $product]);
    }
    public function viewCategories()
    {
        $categories = Category::all();
        return view('home/category_view',['categories' => $categories]);
    }
    public function viewCategoryProducts($id)
    {
        $category = Category::find($id);
        $categories = Category::all();
        $products = Product::where('category_id', $id)->get();
        return view('home/category_view',[
            'products' => $products,'category'=>$category
            ,'categories' => $categories]);
    }

    public function admin(){
        $this->authorize('admin');
        return view('admin');
    }

    public function userProfile(){
        return view('home/user_profile');

    }

    public function userOrder()
    {
        $user=Auth::User();
        $orders = Order::where('user_id',$user->id)->get();
        return view('home/user_order',['orders' => $orders]);
    }


}
