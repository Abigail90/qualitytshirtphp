<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

use App\Http\Requests;

class OrderController extends Controller
{
    public function index()
    {
        $this->authorize('admin');
        return view('order.index',[
            'orders' => Order::all()
        ]);
    }

    public function shipped($id)
    {
        $order=Order::find($id);

        $order->status='Shipped';
        $order->save();

        return redirect('order');

    }

    public function __construct()
    {
        $this->middleware('auth');
    }


}
