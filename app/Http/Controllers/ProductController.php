<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\Supplier;
use Illuminate\Http\Request;

use App\Http\Requests;


class ProductController extends Controller
{
    public function index(){

        $this->authorize('admin');
        $products = Product::orderBy('created_at', 'asc')->get();
        return view('product/index',[

            'products'=>Product::all()
        ]);
    }

    public function store(Request $request)
    {
        $this->authorize('admin');
        $this->validate($request, [
            'name' => 'required|max:255',
            'price' =>'required'

        ]);


        $product = new Product();
        $product->name = $request->name;
        $product->price=$request->price;
        $product->category_id=$request->category;
        $product->supplier_id=$request->supplier;


        if($request->hasFile('file')){

            $file = $request->file('file');
            $product->picture='uploads/'.$file->getClientOriginalName();
            $file->move('uploads', $file->getClientOriginalName());

        }
        $product->save();

        return redirect('product');

    }

    public function create()
    {
        $this->authorize('admin');
        return view('product/create',[

            'categories'=>Category::lists('name','id'),'suppliers'=>Supplier::lists('name','id')
        ]);
    }

    public function delete($id)
    {
        $this->authorize('admin');
        $product = Product::find($id);
        $product->delete();
        return redirect('product');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }



}
