<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Http\Request;


    Route::auth();
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');
    Route::get('/contact', 'HomeController@contactPage');
    Route::post('/authenticate','Auth\AuthController@authenticate');
    Route::resource('category','CategoryController');
    Route::get('/categories', 'HomeController@viewCategories');
    Route::get('/view_category/{id}', 'HomeController@viewCategoryProducts');
    Route::get('category/delete/{id}', 'CategoryController@delete');
    Route::resource('supplier','SupplierController');
    Route::get('supplier/delete/{id}', 'SupplierController@delete');
    Route::resource('product','ProductController');
    Route::get('/view_product/{id}', 'HomeController@viewProduct');
    Route::get('product/delete/{id}', 'ProductController@delete');
    Route::resource('user', 'UserController');
    Route::get('user/enable/{id}', 'UserController@enable');
    Route::get('user/disable/{id}', 'UserController@disable');
    Route::get('/admin', [ 'middleware' => 'auth','uses'=>'HomeController@admin']);
    Route::get('/profile', [ 'middleware' => 'auth','uses'=>'HomeController@userProfile']);
    Route::get('/add_cart/{id}', 'ShoppingCartController@add');
    Route::get('/remove_cart/{id}', 'ShoppingCartController@remove');
    Route::get('/clear_cart', 'ShoppingCartController@clear');
    Route::get('/cart', 'ShoppingCartController@index');
    Route::get('/checkout',[ 'middleware' => 'auth','uses'=>'ShoppingCartController@checkout']);
    Route::resource('/order', 'OrderController');
    Route::get('/order/shipped/{id}', 'OrderController@shipped');
    Route::get('/user_order',[ 'middleware' => 'auth','uses'=>'HomeController@userOrder'] );

