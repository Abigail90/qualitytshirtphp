<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'username' => 'admin',
            'email' => 'admin@123.com',
            'password' => '123456',

        ]);

        // create category
        DB::table('categories')->insert([
            'name' => 'Men',

        ]);
        DB::table('categories')->insert([
            'name' => 'Women',

        ]);
        DB::table('categories')->insert([
            'name' => 'Kids',

        ]);
    }
}


