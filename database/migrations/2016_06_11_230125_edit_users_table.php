<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('type');
            $table->string('phone');
            $table->string('address');
            $table->string('post');
            $table->string('city');
            $table->string('country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('type');
            $table->dropColumn('phone');
            $table->dropColumn('address');
            $table->dropColumn('post');
            $table->dropColumn('city');
            $table->dropColumn('country');
        });
    }
}
