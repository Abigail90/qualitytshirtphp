<?php
/**
 * Created by PhpStorm.
 * User: huizi
 * Date: 6/06/16
 * Time: 17:49
 */?>

@extends('layouts.master')

@section('content')

    <p>
        <a href="/product/create">Create new</a>
    </p>
    <table class="table">
        <tr>
            <th>
                Name
            </th>
            <th>
                Category
            </th>
            <th>
                Supplier
            </th>
            <th>
                Description
            </th>
            <th>
                Price
            </th>
            <th>
                Picture
            </th>
            <th></th>





        @foreach ($products as $product)
            <tr>
                <td>
                    <div>{{ $product->name }}</div>
                </td>
                <td>
                    <div>{{ $product->category['name'] }}</div>
                </td>
                <td>
                    <div>{{ $product->supplier['name']}}</div>
                </td>
                <td>
                    <div>{{ $product->description }}</div>
                </td>
                <td>
                    <div>{{ $product->price }}</div>
                </td>
                <td class="table-text">
                    <div><img src="{{ $product->picture }}"  style="height:228px;"></div>
                </td>
                <td>
                    <a href="/product/delete/{{ $product->id }}">Delete</a>
                </td>
            </tr>
        @endforeach

    </table>


@endsection


