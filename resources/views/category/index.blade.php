<?php
/**
 * Created by PhpStorm.
 * User: huizi
 * Date: 6/06/16
 * Time: 16:03
 */?>

    @extends('layouts.master')

@section('content')

    <h2>Category</h2>

    <p>
        <a href="/category/create">Create new</a>
    </p>
    <table class="table">
        <tr>
            <th>
                Name
            </th>

            <th></th>
        </tr>


        @foreach ($categories as $category)
            <tr>
                <td>
                    <div>{{ $category->name }}</div>
                </td>
                <td>
                    <a href="/category/delete/{{ $category->id }}">Delete</a>
                </td>
            </tr>
        @endforeach

    </table>
@endsection
