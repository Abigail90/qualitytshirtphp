<?php
/**
 * Created by PhpStorm.
 * User: huizi
 * Date: 12/06/16
 * Time: 19:11
 */?>
@extends('layouts.master')

@section('content')



    <div class="panel-body">
        <h2>User</h2>
        <table class="table">
            <tr>
                <th>
                    Name
                </th>
                <th>
                    User Name
                </th>
                <th>
                    Email
                </th>
                <th>
                    Phone
                </th>
                <th>
                    Address
                </th>
                <th>
                    Post
                </th>
                <th>
                    City
                </th>
                <th>
                    Country
                </th>
                <th>
                    Type
                </th>
                <th>
                    Status
                </th>


                <th></th>
            </tr>


            @foreach ($users as $user)
                <tr>
                    <td>
                        <div>{{ $user->name }}</div>
                    </td>
                    <td>
                        <div>{{ $user->username }}</div>
                    </td>
                    <td>
                        <div>{{ $user->email }}</div>
                    </td>
                    <td>
                        <div>{{ $user->phone }}</div>
                    </td>
                    <td>
                        <div>{{ $user->address }}</div>
                    </td>
                    <td>
                        <div>{{ $user->post }}</div>
                    </td><td>
                        <div>{{ $user->city }}</div>
                    </td><td>
                        <div>{{ $user->country }}</div>
                    </td>
                    <td>
                        <div>{{ $user->type }}</div>
                    </td>
                    <td>
                        @if($user->active)
                            Enabled
                        @else
                            Disabled
                        @endif
                    </td>
                    <td>
                        |
                        @if($user->active)
                            <a href="/user/disable/{{ $user->id }}">Disable</a>
                        @else
                            <a href="/user/enable/{{ $user->id }}">Enable</a>
                        @endif
                    </td>


                </tr>
            @endforeach

        </table>
    </div>
@endsection
