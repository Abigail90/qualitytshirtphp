<?php
/**
 * Created by PhpStorm.
 * User: huizi
 * Date: 15/06/16
 * Time: 14:41
 */?>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Branding Image -->
            <a href="{{ url('/home') }}">
                <img alt="Home" src="{{asset('/image/logo.png')}}" width="100px" height="50px">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="{{ url('/home') }}">Home<span class="sr-only">(current)</span></a></li>
                <li><a href="{{ url('/contact') }}">Contact</a></li>



            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/cart') }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i>My Cart</a></li>
                @if(Auth::user())
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Management<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/profile') }}">My profile</a></li>
                        <li><a href="{{ url('/user_order') }}">My Orders</a></li>
                            @if(Auth::user()->type==='admin')
                                <li><a href="{{ url('/admin') }}">Administration</a></li>
                            @endif
                    </ul>
                </li>
                @endif
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


