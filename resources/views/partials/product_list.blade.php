<?php
/**
 * Created by PhpStorm.
 * User: huizi
 * Date: 15/06/16
 * Time: 15:59
 */?>

@foreach ($products as $product)
    <div style="display: inline">
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <a href="/view_product/{{$product->id}}" ><img src="{{ $product->picture }}" style="width:200px;height:300px"></a>
                <div class="caption">
                    <div style="font: bold 20px Calibri, serif">{{ $product->name }}</div>
                    <div style="font-style:oblique">${{$product->price}}</div>
                    <div class="clearfix">{{--<a href="#" class="btn btn-primary" role="button">Detail</a>--}}
                        <a href="/add_cart/{{$product->id}}" id="{{$product->id}}" class="btn btn-default pull-right" role="button">Add to Cart</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endforeach


