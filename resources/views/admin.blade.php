<?php
/**
 * Created by PhpStorm.
 * User: huizi
 * Date: 12/06/16
 * Time: 14:37
 */?>
@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Admin management</div>

                    <div class="panel-body">
                        <p>
                            <a href="/product">Manage product</a>
                        </p>
                        <p>
                            <a href="/category">Manage category</a>
                        </p>
                        <p>
                            <a href="/supplier">Manage supplier</a>
                        </p>
                        <p>
                            <a href="/user">Manage user</a>
                        </p>
                        <p>
                            <a href="/order">Manage order</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

