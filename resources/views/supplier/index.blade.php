<?php
/**
 * Created by PhpStorm.
 * User: huizi
 * Date: 6/06/16
 * Time: 16:57
 */?>

@extends('layouts.master')

@section('content')

    <h2>Supplier</h2>

    <p>
        <a href="/supplier/create">Create new</a>
    </p>
    <table class="table">
        <tr>
            <th>
                Name
            </th>


            <th>
                Email Address
            </th>
            <th>
                Phone Number
            </th>
        <th></th>





        @foreach ($suppliers as $supplier)
            <tr>
                <td>
                    <div>{{ $supplier->name }}</div>
                </td>
                <td>
                    <div>{{ $supplier->email }}</div>
                </td>
                <td>
                    <div>{{ $supplier->phoneNumber }}</div>
                </td>
                <td>|
                    <a href="/supplier/delete/{{ $supplier->id }}">Delete</a>
                </td>
            </tr>
        @endforeach

    </table>


@endsection

