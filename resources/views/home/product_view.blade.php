<?php
/**
 * Created by PhpStorm.
 * User: huizi
 * Date: 12/06/16
 * Time: 13:53
 */?>

@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>{{ $product->name }}</h2></div>

                    <div class="panel-body" align="center">
                        <div><img src="/{{ $product->picture }}" ></div>

                        <dl class="dl-horizontal">
                            <dt>
                                Description
                            </dt>

                            <dd>
                                <div>{{ $product->description }}</div>
                            </dd>

                            <dt>
                                Price
                            </dt>

                            <dd>
                                <div>${{ $product->price }}</div>
                            </dd>


                        </dl>
                        <div><a href="#" id="{{$product->id}}" class="btn btn-primary">Add to Cart</a></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
