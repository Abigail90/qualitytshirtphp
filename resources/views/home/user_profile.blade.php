<?php
/**
 * Created by PhpStorm.
 * User: huizi
 * Date: 12/06/16
 * Time: 15:06
 */?>
@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">My Profile</div>

                    <div class="panel-body">
                        <dl class="dl-horizontal">


                            <dt>
                                Username
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->username }}</div>
                            </dd>

                            <dt>
                                Email
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->email }}</div>
                            </dd>

                            <dt>
                                Phone
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->phone }}</div>
                            </dd>

                            <dt>
                                Name
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->name }}</div>
                            </dd>

                            <dt>
                                address
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->address }}</div>
                            </dd>

                            <dt>
                                Postal Code
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->post }}</div>
                            </dd>


                            <dt>
                                City
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->city }}</div>
                            </dd>
                            <dt>
                                Country
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->country }}</div>
                            </dd>




                        </dl>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
