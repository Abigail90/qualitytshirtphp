<?php
/**
 * Created by PhpStorm.
 * User: huizi
 * Date: 12/06/16
 * Time: 13:56
 */?>

@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col-sm-3">
                    <div><h2>Categories</h2></div>
                    <div class="sidebar-nav">
                        <nav>
                            <ul class="nav">
                                @foreach ($categories as $category)
                                    <li><a href="/view_category/{{$category->id}}">{{$category->name}}</a></li>

                                @endforeach


                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">{{$category->name}}</div>


                        <div style="display: inline-block">
                            @foreach ($products as $product)
                                <div style="display: inline-block;padding:2em">

                                    <div><a href="/view_product/{{$product->id}}"  ><img src="/{{ $product->picture }}" style="width:250px;"></a></div>
                                    <div style="font: bold 20px Georgia, serif">{{ $product->name }}</div>
                                    <div style="font-style:oblique">${{$product->price}}</div>


                                </div>
                            @endforeach

                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
